/*
 * led_handler.c
 *
 *  Created on: 02.07.2017
 *      Author: schnake
 */


#include "coap.h"
#include "cJSON.h"

#include "coap_helper.h"

//#include "driver/gpio.h"
//#include "driver/rtc_io.h"
#include "driver/ledc.h"
#include "esp_log.h"

#include "led.h"
#include "led_handler.h"



void init_led_handler()
{
}

void rgbwGetHandler(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response)
{
	ESP_LOGD(__FUNCTION__, "Entered RGBW get handler");

	cJSON *array = cJSON_CreateArray();
	cJSON *item;
	for(int i=0; i<getLedCount(); ++i) {
		const rgbw_t *rgbw = getLedRgbw(i);
		item = cJSON_CreateObject();
		cJSON_AddNumberToObject(item, "i", i);
		cJSON_AddNumberToObject(item, "r", rgbw->r);
		cJSON_AddNumberToObject(item, "g", rgbw->g);
		cJSON_AddNumberToObject(item, "b", rgbw->b);
		cJSON_AddNumberToObject(item, "w", rgbw->w);
		cJSON_AddNumberToObject(item, "brightness", rgbw->brightness);
		cJSON_AddNumberToObject(item, "options", rgbw->options);
		cJSON_AddItemToObject(array, "item", item);
	}
	char *jsonString = cJSON_PrintUnformatted(array);
	ESP_LOGD(__FUNCTION__, "JSON string created");

	unsigned char buf[3];
	response->hdr->code = COAP_RESPONSE_CODE(205);
	coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_APPLICATION_JSON), buf);
	coap_add_data(response, strlen(jsonString), (unsigned char*)jsonString);
	ESP_LOGD(__FUNCTION__, "Response generated");

	free(jsonString);
	cJSON_Delete(array);
	ESP_LOGD(__FUNCTION__, "free cJSON objects");
}

void rgbwPutHandler(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response)
{
	ESP_LOGD(__FUNCTION__, "Entered RGBW get handler");

	char *data = getPduData(request);
	if(data) {
		ESP_LOGV(__FUNCTION__, "String: %s", data);
		cJSON *root = cJSON_Parse(data);
		if(root) {
			ESP_LOGV(__FUNCTION__, "ROOT found, search items");
			int16_t i;
			cJSON *ji = cJSON_GetObjectItem(root, "i");
			cJSON *jr = cJSON_GetObjectItem(root, "r");
			cJSON *jg = cJSON_GetObjectItem(root, "g");
			cJSON *jb = cJSON_GetObjectItem(root, "b");
			cJSON *jw = cJSON_GetObjectItem(root, "w");
			cJSON *jbrightness = cJSON_GetObjectItem(root, "brightness");
			cJSON *joptions = cJSON_GetObjectItem(root, "options");
			ESP_LOGV(__FUNCTION__, "Items: %p %p %p %p %p %p", ji, jr, jg, jb, jw, jbrightness);

			if(ji && ji->type==cJSON_Number) {
				i = ji->valueint;
				ESP_LOGV(__FUNCTION__, "For index: %d", i);
				if(i>=0 && i <getLedCount()) {
					if(jr && jr->type==cJSON_Number) {
						setLedRed(i, jr->valueint);
					}
					if(jg && jg->type==cJSON_Number) {
						setLedGreen(i, jg->valueint);
					}
					if(jb && jb->type==cJSON_Number) {
						setLedBlue(i, jb->valueint);
					}
					if(jw && jw->type==cJSON_Number) {
						setLedWhite(i, jw->valueint);
					}
					if(jbrightness && jbrightness->type==cJSON_Number){
						setLedBrightness(i, jbrightness->valueint);
					}
					if(joptions && joptions->type==cJSON_Number) {
						setLedOptions(i, joptions->valueint);
					}
					setLedDuty(i);
					response->hdr->code = COAP_RESPONSE_CODE(205);
				} else {
					response->hdr->code = COAP_RESPONSE_405;
				}
			} else {
				response->hdr->code = COAP_RESPONSE_405;
			}
			cJSON_Delete(root);
		} else {
			response->hdr->code = COAP_RESPONSE_400;
		}
		free(data);
	} else {
		response->hdr->code = COAP_RESPONSE_400;
	}

	response->hdr->code = COAP_RESPONSE_CODE(205);
}
