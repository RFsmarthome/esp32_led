/*
 * wifi.h
 *
 *  Created on: 11.07.2017
 *      Author: schnake
 */

#ifndef MAIN_WIFI_H_
#define MAIN_WIFI_H_

#ifdef __cplusplus
extern "C" {
#endif

void init_wifi();
void smartconfigTask(void *arg);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_WIFI_H_ */
