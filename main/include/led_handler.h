/*
 * led_handler.h
 *
 *  Created on: 02.07.2017
 *      Author: schnake
 */

#ifndef MAIN_INCLUDE_LED_HANDLER_H_
#define MAIN_INCLUDE_LED_HANDLER_H_

#ifdef __cplusplus
extern "C" {
#endif

void init_led_handler();

void rgbwGetHandler(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response);

void rgbwPutHandler(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_INCLUDE_LED_HANDLER_H_ */
