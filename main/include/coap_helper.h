/*
 * coap_helper.h
 *
 *  Created on: 28.05.2017
 *      Author: schnake
 */

#ifndef MAIN_COAP_HELPER_H_
#define MAIN_COAP_HELPER_H_

#ifdef __cplusplus
extern "C" {
#endif

char* getPduData(coap_pdu_t *pdu);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_COAP_HELPER_H_ */
