/*
 * key.h
 *
 *  Created on: 02.07.2017
 *      Author: schnake
 */

#ifndef MAIN_INCLUDE_KEY_H_
#define MAIN_INCLUDE_KEY_H_

#ifdef __cplusplus
extern "C" {
#endif

void init_key();
int getFactoryResetKey();
void keyPressedTask(void *arg);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_INCLUDE_KEY_H_ */
