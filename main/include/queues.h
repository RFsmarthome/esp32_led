/*
 * queues.h
 *
 *  Created on: 02.07.2017
 *      Author: schnake
 */

#ifndef MAIN_INCLUDE_QUEUES_H_
#define MAIN_INCLUDE_QUEUES_H_


#define CONNECTED_BIT 0x1
#define ESPTOUCH_DONE_BIT 0x2
#define SMARTCONFIG_RUNNING_BIT 0x4


#ifdef __cplusplus
extern "C" {
#endif

void init_queues();
xQueueHandle getGpioQueueHandle();
EventGroupHandle_t getAppEventGroupHandle();

#ifdef __cplusplus
}
#endif

#endif /* MAIN_INCLUDE_QUEUES_H_ */
