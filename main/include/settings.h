/*
 * settings.h
 *
 *  Created on: 28.05.2017
 *      Author: schnake
 */

#ifndef MAIN_SETTINGS_H_
#define MAIN_SETTINGS_H_

#ifdef __cplusplus
extern "C" {
#endif

void settingsGetHandle(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response);

void settingsPutHandle(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_SETTINGS_H_ */
