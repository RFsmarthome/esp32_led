/*
 * led.h
 *
 *  Created on: 31.05.2017
 *      Author: schnake
 */

#ifndef MAIN_LED_H_
#define MAIN_LED_H_

#define CHANNEL_WHITE LEDC_CHANNEL_0
#define CHANNEL_RED LEDC_CHANNEL_1
#define CHANNEL_GREEN LEDC_CHANNEL_2
#define CHANNEL_BLUE LEDC_CHANNEL_3

#define GPIO_WHITE GPIO_NUM_16
#define GPIO_RED GPIO_NUM_17
#define GPIO_GREEN GPIO_NUM_18
#define GPIO_BLUE GPIO_NUM_19

typedef struct {
	int16_t r, g, b, w, brightness;
	int16_t options;
} rgbw_t;

#ifdef __cplusplus
extern "C" {
#endif

void init_led();
//void setLedDuty(ledc_channel_t channel, int value);

void setLedGamma(float g);
float getLedGamma();

void setLedRgbw(const int index, const rgbw_t *rgbw);
const rgbw_t* getLedRgbw();

void setLedRed(const int index, const int r);
void setLedGreen(const int index, const int g);
void setLedBlue(const int index, const int b);
void setLedWhite(const int index, const int w);
void setLedBrightness(const int index, const int brightness);
void setLedOptions(const int index, const int options);

int getLedRed(const int index);
int getLedGreen(const int index);
int getLedBlue(const int index);
int getLedWhite(const int index);
int getLedBrightness(const int index);
int getLedOptions(const int index);

int getLedCount();

void setLedDuty(const int index);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_LED_H_ */
