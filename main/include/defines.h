/*
 * led_defines.h
 *
 *  Created on: 28.05.2017
 *      Author: schnake
 */

#ifndef MAIN_LED_DEFINES_H_
#define MAIN_LED_DEFINES_H_

#define NVS_NAMESPACE "led"
#define NVS_WIFI_SSID "ssid"
#define NVS_WIFI_PASSWORD "password"
#define NVS_DESCRIPTION "description"

#define MDNS_SERVICE "_sbcoap"
#define MDNS_PROTO "_udp"

//#define COAP_RESOURCE_CHECK_TIME 30

#endif /* MAIN_LED_DEFINES_H_ */
