/*
 * coap-server.h
 *
 *  Created on: 24.05.2017
 *      Author: schnake
 */

#ifndef MAIN_COAP_SERVER_H_
#define MAIN_COAP_SERVER_H_

#ifdef __cplusplus
extern "C" {
#endif

void coapServer(void *p);

#ifdef __cplusplus
}
#endif

#endif /* MAIN_COAP_SERVER_H_ */
