#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_log.h"
#include "esp_wifi_types.h"
#include "esp_wifi.h"
#include "esp_smartconfig.h"
#include "esp_event_loop.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "lwip/inet.h"
#include "tcpip_adapter.h"
#include "mdns.h"

#include "defines.h"
#include "wifi.h"
#include "queues.h"

#define COAP_DEFAULT_PORT 5683
#define DESC_DEFAULT "desc=[nichts]"

#ifndef VERSION
#define VERSION "------"
#endif



static char* setDefaultDescription()
{
	ESP_LOGV(__FUNCTION__, "Set to default description [nichts]");
	char *description = (char*)malloc(sizeof(DESC_DEFAULT) + 1);
	strcpy(description, DESC_DEFAULT);
	return description;
}

static esp_err_t wifiEventHandler(void *ctx, system_event_t *event)
{
	switch(event->event_id) {
		case SYSTEM_EVENT_STA_START:
			ESP_LOGD(__FUNCTION__, "Event STA start");
			if(!(xEventGroupGetBits(getAppEventGroupHandle()) & SMARTCONFIG_RUNNING_BIT)) {
				ESP_LOGD(__FUNCTION__, "Try to connect");
				esp_wifi_connect();
			} else {
				ESP_LOGD(__FUNCTION__, "In smartconfig mode, no connect");
			}
			break;
		case SYSTEM_EVENT_STA_GOT_IP:
			ESP_LOGD(__FUNCTION__, "Event STA got IP");
			ESP_LOGD(__FUNCTION__, "Got IP: %s/%s", ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip), ip4addr_ntoa(&event->event_info.got_ip.ip_info.netmask));
			xEventGroupSetBits(getAppEventGroupHandle(), CONNECTED_BIT);
			break;
		case SYSTEM_EVENT_STA_DISCONNECTED:
			ESP_LOGD(__FUNCTION__, "Event STA disconnected")
			xEventGroupClearBits(getAppEventGroupHandle(), CONNECTED_BIT);
			if(!(xEventGroupGetBits(getAppEventGroupHandle()) & SMARTCONFIG_RUNNING_BIT)) {
				ESP_LOGD(__FUNCTION__, "Try to connect");
				esp_wifi_connect();
			} else {
				ESP_LOGD(__FUNCTION__, "In smartconfig mode, no connect");
			}
			break;
		default:
			break;
	}
	return ESP_OK;
}

void init_mdns()
{
	mdns_server_t *mdns = NULL;

	nvs_handle nvsHandle;
	esp_err_t rc;
	char *description;

	rc = nvs_open(NVS_NAMESPACE, NVS_READONLY, &nvsHandle);
	if(rc==ESP_OK) {
		ESP_LOGD(__FUNCTION__, "nvs_open successful");
		size_t lenDescription;
		ESP_LOGV(__FUNCTION__, "Check description for MDNS");
		rc = nvs_get_str(nvsHandle, NVS_DESCRIPTION, NULL, &lenDescription);
		if(rc==ESP_OK) {
			char *nvsDesc;
			ESP_LOGD(__FUNCTION__, "Description found, length: %d", lenDescription);
			nvsDesc = (char*)malloc(lenDescription+1);
			rc = nvs_get_str(nvsHandle, NVS_DESCRIPTION, nvsDesc, &lenDescription);
			if(rc==ESP_OK) {
				description = (char*)malloc(lenDescription+1+sizeof("desc="));
				sprintf(description,"desc=%s", nvsDesc);
			} else {
				description = setDefaultDescription();
			}
			free(nvsDesc);
		} else {
			description = setDefaultDescription();
		}
	} else {
		description = setDefaultDescription();
	}
	nvs_close(nvsHandle);

	uint8_t mac[6];
	ESP_ERROR_CHECK(esp_wifi_get_mac((wifi_interface_t )TCPIP_ADAPTER_IF_STA, mac));
	char* hostname;
	hostname = (char*) malloc(20);
	sprintf(hostname, "SB%x%x%x%x%x%x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	ESP_ERROR_CHECK(tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, hostname));
	ESP_LOGI("init_wifi", "Set to hostname %s", hostname);
	ESP_LOGD(__FUNCTION__, "wait for connection");
	xEventGroupWaitBits(getAppEventGroupHandle(), CONNECTED_BIT, true, false, portMAX_DELAY);
	ESP_LOGD(__FUNCTION__, "connected");
	ESP_ERROR_CHECK(mdns_init(TCPIP_ADAPTER_IF_STA, &mdns));
	ESP_ERROR_CHECK(mdns_set_hostname(mdns, hostname));
	ESP_ERROR_CHECK(mdns_service_add(mdns, MDNS_SERVICE, MDNS_PROTO, COAP_DEFAULT_PORT));
	ESP_ERROR_CHECK(mdns_service_port_set(mdns, MDNS_SERVICE, MDNS_PROTO, COAP_DEFAULT_PORT));
	ESP_LOGV(__FUNCTION__, "MDNS Description: %s", description);
	const char* txt[1];
	txt[0] = description;
	ESP_ERROR_CHECK(mdns_service_txt_set(mdns, MDNS_SERVICE, MDNS_PROTO, 1, txt));
	ESP_LOGD("init_wifi", "mdns server enable for _sbcoap._udp service");
	free(description);
}

void init_wifi()
{
	ESP_LOGI("init_wifi", "Init WiFi and TCP/IP");

	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(wifiEventHandler, NULL));
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	//ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_FLASH));
	ESP_ERROR_CHECK(esp_wifi_set_country(WIFI_COUNTRY_EU));
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	esp_wifi_set_ps(WIFI_PS_MODEM);
	esp_wifi_set_auto_connect(true);
	ESP_ERROR_CHECK(esp_wifi_start());
	esp_wifi_connect();

	init_mdns();
}

static void sc_callback(smartconfig_status_t status, void *pdata)
{
    switch (status) {
        case SC_STATUS_WAIT:
            ESP_LOGI(__FUNCTION__, "SC_STATUS_WAIT");
            break;
        case SC_STATUS_FIND_CHANNEL:
            ESP_LOGI(__FUNCTION__, "SC_STATUS_FINDING_CHANNEL");
            break;
        case SC_STATUS_GETTING_SSID_PSWD:
            ESP_LOGI(__FUNCTION__, "SC_STATUS_GETTING_SSID_PSWD");
            break;
        case SC_STATUS_LINK:
            ESP_LOGI(__FUNCTION__, "SC_STATUS_LINK");
            wifi_config_t *wifi_config = pdata;
            ESP_LOGI(__FUNCTION__, "SSID:%s", wifi_config->sta.ssid);
            ESP_LOGI(__FUNCTION__, "PASSWORD:%s", wifi_config->sta.password);
            ESP_ERROR_CHECK( esp_wifi_disconnect() );
            ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, wifi_config) );
            ESP_ERROR_CHECK( esp_wifi_connect() );
            break;
        case SC_STATUS_LINK_OVER:
            ESP_LOGI(__FUNCTION__, "SC_STATUS_LINK_OVER");
            if (pdata != NULL) {
                uint8_t phone_ip[4] = { 0 };
                memcpy(phone_ip, (uint8_t* )pdata, 4);
                ESP_LOGI(__FUNCTION__, "Phone ip: %d.%d.%d.%d\n", phone_ip[0], phone_ip[1], phone_ip[2], phone_ip[3]);
            }
            xEventGroupSetBits(getAppEventGroupHandle(), ESPTOUCH_DONE_BIT);
            break;
        default:
            break;
    }
}

void smartconfigTask(void *arg)
{
	xEventGroupSetBits(getAppEventGroupHandle(), SMARTCONFIG_RUNNING_BIT);
	esp_wifi_disconnect();
	/*
	esp_wifi_restore();
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	//ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_FLASH));
	ESP_ERROR_CHECK(esp_wifi_set_country(WIFI_COUNTRY_EU));
	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	esp_wifi_set_auto_connect(true);
	ESP_ERROR_CHECK(esp_wifi_start());
	*/

	esp_smartconfig_set_type(SC_TYPE_ESPTOUCH);
	esp_smartconfig_start(sc_callback);

	EventBits_t uxBits;

	while(1) {
		uxBits = xEventGroupWaitBits(getAppEventGroupHandle(), CONNECTED_BIT | ESPTOUCH_DONE_BIT, true, false, portMAX_DELAY);
		if(uxBits & CONNECTED_BIT) {
			ESP_LOGI(__FUNCTION__, "WiFi connected to ap");
		}
		if(uxBits & ESPTOUCH_DONE_BIT) {
			ESP_LOGI(__FUNCTION__, "smartconfig over");
			esp_smartconfig_stop();
			xEventGroupClearBits(getAppEventGroupHandle(), SMARTCONFIG_RUNNING_BIT);
			vTaskDelete(NULL);
		}
	}
}
