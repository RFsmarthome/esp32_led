
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#include "esp_log.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "defines.h"
#include "led.h"
#include "key.h"
#include "queues.h"
#include "wifi.h"
#include "coap_server.h"


void app_main(void)
{
	nvs_flash_init();

	ESP_LOGI(__FUNCTION__, "Schnakebus LED controller, build %s", VERSION);
	ESP_LOGD("app_main", "START");

	init_queues();
	init_key();

	init_led();

	init_wifi();
	coapServer(NULL);
}
