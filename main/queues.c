/*
 * queues.c
 *
 *  Created on: 02.07.2017
 *      Author: schnake
 */

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"


static xQueueHandle g_gpio_queue = NULL;
static EventGroupHandle_t wifiEventGroup;

void init_queues()
{
	g_gpio_queue = xQueueCreate(10, sizeof(uint32_t));
	wifiEventGroup = xEventGroupCreate();
}

xQueueHandle getGpioQueueHandle()
{
	return g_gpio_queue;
}

EventGroupHandle_t getAppEventGroupHandle()
{
	return wifiEventGroup;
}
