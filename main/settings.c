/*
 * settings.c
 *
 *  Created on: 28.05.2017
 *      Author: schnake
 */

#include <defines.h>
#include "stdint.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "nvs.h"
#include "cJSON.h"
#include "coap.h"

#include "driver/ledc.h"

#include "led.h"
#include "coap_helper.h"

#include "settings.h"

void settingsGetHandle(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response)
{
	ESP_LOGD(__FUNCTION__, "settingsGetHandle");

	nvs_handle nvsHandle;
	cJSON *root = cJSON_CreateObject();

	esp_err_t rc = nvs_open(NVS_NAMESPACE, NVS_READONLY, &nvsHandle);
	if(rc==ESP_OK) {
		size_t len;
		rc = nvs_get_str(nvsHandle, NVS_WIFI_SSID, NULL, &len);
		if(rc==ESP_OK) {
			++len;
			char *ssid = malloc(len);
			ESP_ERROR_CHECK(nvs_get_str(nvsHandle, NVS_WIFI_SSID, ssid, &len));
			cJSON_AddStringToObject(root, "ssid", ssid);
			free(ssid);
		}

		rc = nvs_get_str(nvsHandle, NVS_DESCRIPTION, NULL, &len);
		if(rc==ESP_OK) {
			++len;
			char *description = malloc(len);
			ESP_ERROR_CHECK(nvs_get_str(nvsHandle, NVS_DESCRIPTION, description, &len));
			cJSON_AddStringToObject(root, "desc", description);
			free(description);
		}
		nvs_close(nvsHandle);
	} else {
		cJSON_AddStringToObject(root, "ssid", "default");
	}

	cJSON_AddNumberToObject(root, "gamma", getLedGamma());
	cJSON_AddStringToObject(root, "version", VERSION);

	char *jsonString = cJSON_PrintUnformatted(root);

	unsigned char buf[3];
	response->hdr->code = COAP_RESPONSE_CODE(205);
	coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_APPLICATION_JSON), buf);
	coap_add_data(response, strlen(jsonString), (unsigned char*)jsonString);
	ESP_LOGD(__FUNCTION__, "Response generated");

	free(jsonString);
	cJSON_Delete(root);
}

void restartTask(void *param) {
	vTaskDelay(2000 / portTICK_PERIOD_MS);
	esp_restart();
	return;
}

void settingsPutHandle(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response)
{
	char *data = getPduData(request);
	if(data) {
		char restart = 0;
		cJSON *root = cJSON_Parse(data);
		if(root) {
			nvs_handle nvsHandle;
			ESP_ERROR_CHECK(nvs_open(NVS_NAMESPACE, NVS_READWRITE, &nvsHandle));

			cJSON *ssid = cJSON_GetObjectItem(root, "ssid");
			cJSON *password = cJSON_GetObjectItem(root, "password");

			if(ssid && password) {
				ESP_LOGD(__FUNCTION__, "ssid: %s", ssid->valuestring);
				ESP_LOGD(__FUNCTION__, "password: %s", password->valuestring);

				nvs_set_str(nvsHandle, NVS_WIFI_SSID, ssid->valuestring);
				nvs_set_str(nvsHandle, NVS_WIFI_PASSWORD, password->valuestring);

				restart = 1;
			}

			cJSON *gamma = cJSON_GetObjectItem(root, "gamma");
			if(gamma) {
				setLedGamma(gamma->valuedouble);
				ESP_LOGD(__FUNCTION__, "gamma: %f", getLedGamma());
			}

			cJSON *description = cJSON_GetObjectItem(root, "desc");
			if(description) {
				ESP_LOGD(__FUNCTION__, "description: %s", description->valuestring);
				nvs_set_str(nvsHandle, NVS_DESCRIPTION, description->valuestring);
				restart = 1;
			}
			cJSON_Delete(root);

			if(restart) {
				nvs_commit(nvsHandle);
				nvs_close(nvsHandle);

				ESP_LOGD(__FUNCTION__, "Restart ESP");
				TaskHandle_t taskHandle;
				xTaskCreate(restartTask, "RestartTask", 2048, NULL, tskIDLE_PRIORITY, &taskHandle);
			}
		}
		free(data);
	}
}

