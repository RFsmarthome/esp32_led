/*
 * coap-server.c
 *
 *  Created on: 24.05.2017
 *      Author: schnake
 */
#include <coap_server.h>
#include <defines.h>
#include "coap.h"
#include "esp_log.h"
#include "cJSON.h"
#include "driver/adc.h"
#include "driver/gpio.h"
#include "driver/ledc.h"
#include "nvs.h"

#include "led.h"
#include "led_handler.h"
#include "coap_helper.h"
#include "settings.h"

#define COAP_INADDR_ALL_NODES ((u32_t)0xBB0100E0UL)
#define INDEX "This is a schnakebus ESP32 LED controller\n" \
	"Copyright (C) 2017 Marcus Schneider <schnake24@gmail.com>\n\n"

static void hallHandler(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response)
{
	unsigned char buf[3];
	ESP_LOGD(__FUNCTION__, "Entered hallHandler");

	if(coap_find_observer(resource, peer, token)) {
		coap_add_option(response, COAP_OPTION_OBSERVE, coap_encode_var_bytes(buf, ctx->observe), buf);
	}

	cJSON *root;
	root = cJSON_CreateObject();
	ESP_LOGD(__FUNCTION__, "object prepared");
	cJSON_AddNumberToObject(root, "energy", hall_sensor_read());

	char *jsonString = cJSON_PrintUnformatted(root);
	ESP_LOGD(__FUNCTION__, "JSON structure generated");

	ESP_LOGD(__FUNCTION__, "prepare response");
	response->hdr->code = COAP_RESPONSE_CODE(205);
	coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_APPLICATION_JSON), buf);
	coap_add_option(response, COAP_OPTION_MAXAGE, coap_encode_var_bytes(buf, 60), buf);
	coap_add_data(response, strlen(jsonString), (unsigned char*)jsonString);
	ESP_LOGD(__FUNCTION__, "response generated");
	free(jsonString);
	cJSON_Delete(root);
	ESP_LOGD(__FUNCTION__, "free cJSON objects");
}

static void indexHandler(coap_context_t *ctx,
		struct coap_resource_t *resource,
		const coap_endpoint_t *localInterface,
		coap_address_t *peer,
		coap_pdu_t *request,
		str *token,
		coap_pdu_t *response)
{
	unsigned char buf[3];
	response->hdr->code = COAP_RESPONSE_CODE(205);
	coap_add_option(response, COAP_OPTION_CONTENT_TYPE,coap_encode_var_bytes(buf, COAP_MEDIATYPE_TEXT_PLAIN), buf);
	coap_add_option(response, COAP_OPTION_MAXAGE,coap_encode_var_bytes(buf, 0x2ffff), buf);
	coap_add_data(response, strlen(INDEX), (unsigned char *)INDEX);
	ESP_LOGD(__FUNCTION__, "Handler ended");
}

void coapServer(void *p)
{
	coap_context_t *ctx;
	coap_address_t server_address;
	coap_resource_t *r;
	fd_set readfds;


	//coap_set_log_level(LOG_DEBUG);
	// Init led array
	init_led_handler();

	adc1_config_width(ADC_WIDTH_12Bit);

	coap_address_init(&server_address);
	server_address.addr.sin.sin_family = AF_INET;
	server_address.addr.sin.sin_addr.s_addr = INADDR_ANY;
	server_address.addr.sin.sin_port = htons(COAP_DEFAULT_PORT);

	ctx = coap_new_context(&server_address);
	if(!ctx) {
		ESP_LOGE(__FUNCTION__, "Error by initializing coap server context");
		exit(EXIT_FAILURE);
	}

	r = coap_resource_init(NULL, 0, 0);
	coap_register_handler(r, COAP_REQUEST_GET, indexHandler);
	coap_add_attr(r, (unsigned char*)"ct", 2, (unsigned char*)"0", 1, 0);
	coap_add_attr(r, (unsigned char*)"title", 5, (unsigned char*)"\"General info\"", 14, 0);
	coap_add_resource(ctx, r);

	r = coap_resource_init((unsigned char*)"settings", 8, 0);
	coap_register_handler(r, COAP_REQUEST_GET, settingsGetHandle);
	coap_register_handler(r, COAP_REQUEST_PUT, settingsPutHandle);
	coap_add_attr(r, (unsigned char*)"ct", 2, (unsigned char*)"50", 2, 0);
	coap_add_attr(r, (unsigned char*)"title", 5, (unsigned char*)"\"Settings\"", 10, 0);
	coap_add_attr(r, (unsigned char*)"rt", 2, (unsigned char*)"\"JSON settings object\"", 13, 0);
	coap_add_resource(ctx, r);

	coap_resource_t *hall_resource = coap_resource_init((unsigned char*)"hall", 4, 0);
	coap_register_handler(hall_resource, COAP_REQUEST_GET, hallHandler);
	coap_add_attr(hall_resource, (unsigned char*)"ct", 2, (unsigned char*)"50", 2, 0);
	coap_add_attr(hall_resource, (unsigned char*)"title", 5, (unsigned char*)"\"Hall sensor\"", 13, 0);
	coap_add_attr(hall_resource, (unsigned char*)"rt", 2, (unsigned char*)"\"Energy\"", 8, 0);
	hall_resource->observable = 1;
	coap_add_resource(ctx, hall_resource);

	r = coap_resource_init((unsigned char*)"rgbw", 4, 0);
	coap_register_handler(r, COAP_REQUEST_GET, rgbwGetHandler);
	coap_register_handler(r, COAP_REQUEST_PUT, rgbwPutHandler);
	coap_add_attr(r, (unsigned char*)"ct", 2, (unsigned char*)"50", 2, 0);
	coap_add_attr(r, (unsigned char*)"title", 5, (unsigned char*)"\"LED RGBW controller\"", 21, 0);
	coap_add_attr(r, (unsigned char*)"rt", 2, (unsigned char*)"\"RGBW\"", 6, 0);
	coap_add_resource(ctx, r);

	ESP_LOGI(__FUNCTION__, "Join all nodes multicast group");
	ip_mreq mreg;
	mreg.imr_interface.s_addr = INADDR_ANY;
	mreg.imr_multiaddr.s_addr = COAP_INADDR_ALL_NODES;

	int result = setsockopt(ctx->sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreg, sizeof(mreg));
	if(result<0) {
		ESP_LOGE(__FUNCTION__, "Could not join multicast group all nodes. Error %d", result);
	}

	coap_queue_t *nextpdu;
	coap_tick_t now;
	struct timeval tv, *timeout;
	while(1) {
		ESP_LOGV(__FUNCTION__, "Wait COAP msg");
		FD_ZERO(&readfds);
		FD_SET(ctx->sockfd, &readfds);

		nextpdu = coap_peek_next( ctx );

		coap_ticks(&now);
		while(nextpdu && nextpdu->t <= now - ctx->sendqueue_basetime) {
			coap_retransmit( ctx, coap_pop_next( ctx ) );
			nextpdu = coap_peek_next( ctx );
		}

		if(nextpdu && nextpdu->t <= COAP_RESOURCE_CHECK_TIME) {
			tv.tv_usec = ((nextpdu->t) % COAP_TICKS_PER_SECOND) * 1000000 / COAP_TICKS_PER_SECOND;
			tv.tv_sec = (nextpdu->t) / COAP_TICKS_PER_SECOND;
			timeout = &tv;
		} else {
			tv.tv_usec = 0;
			tv.tv_sec = COAP_RESOURCE_CHECK_TIME;
			timeout = &tv;
		}
	    result = select(FD_SETSIZE, &readfds, 0, 0, timeout);
	    //result = select(FD_SETSIZE, &readfds, 0, 0, &tv);

	    if(result < 0) {         /* error */
    		ESP_LOGE(__FUNCTION__, "Socket error %d", result);
			exit(EXIT_FAILURE);
	    } else if(result > 0 && FD_ISSET(ctx->sockfd, &readfds)) {  /* read from socket */
	    	coap_read( ctx );       /* read received data */
	    } else {      /* timeout */
	    	/*
	    	ESP_LOGD(__FUNCTION__, "Timeout hall sensor");
	    	if(hall_resource) {
	    		hall_resource->dirty = 1;
	    	}
	    	*/
	    }

		coap_check_notify(ctx);
	}
}
