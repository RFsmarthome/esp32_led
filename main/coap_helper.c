/*
 * coap_helper.c
 *
 *  Created on: 28.05.2017
 *      Author: schnake
 */

#include "coap.h"

#include "coap_helper.h"

char* getPduData(coap_pdu_t *pdu)
{
	size_t length;
	unsigned char *tmpdata;
	coap_get_data(pdu, &length, &tmpdata);
	if(length>0) {
		char *data = (char*)malloc(length+1);
		strncpy(data, (char*)tmpdata, length);
		data[length] = 0;
		return data;
	}
	return 0;
}
