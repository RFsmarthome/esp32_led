/*
 * key.c
 *
 *  Created on: 02.07.2017
 *      Author: schnake
 */

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_err.h"
#include "esp_log.h"

#include "driver/gpio.h"

#include "key.h"
#include "queues.h"
#include "wifi.h"

#define FACTORY_RESET_GPIO GPIO_NUM_0

/*
static const char *TAG = "key";

static void IRAM_ATTR keypressedHandle(void *arg)
{
	uint32_t param = (uint32_t)arg;
	xQueueSendFromISR(getGpioQueueHandle(), &param, NULL);
}
*/

void init_key()
{
		gpio_config_t factoryResetKey;
		factoryResetKey.mode = GPIO_MODE_INPUT;
		factoryResetKey.pull_up_en = 1;
		factoryResetKey.pull_down_en = 0;
		factoryResetKey.intr_type = GPIO_INTR_HIGH_LEVEL;
		factoryResetKey.pin_bit_mask = (1<<FACTORY_RESET_GPIO); // GPIO0

		ESP_ERROR_CHECK(gpio_config(&factoryResetKey));

		xTaskCreate(keyPressedTask, "keyPressedTask", 2048, NULL, 10, NULL);
}

int getFactoryResetKey()
{
	return gpio_get_level(FACTORY_RESET_GPIO);
}

void keyPressedTask(void *arg)
{
	const TickType_t frequency = 1000 / portTICK_PERIOD_MS;
	int factoryResetCount = 0;
	for(;;) {
		vTaskDelay(frequency);
		if(!getFactoryResetKey()) {
			++factoryResetCount;
		} else {
			factoryResetCount = 0;
		}
		if(factoryResetCount>4) {
			if((xEventGroupGetBits(getAppEventGroupHandle()) & SMARTCONFIG_RUNNING_BIT) == 0) {
				ESP_LOGI(__FUNCTION__, "This should perform a factory reset");
				xTaskCreate(smartconfigTask, "smartconfigTask", 4096, NULL, 3, NULL);
				factoryResetCount = 0;
			} else {
				ESP_LOGI(__FUNCTION__, "Smartconfig already running...");
				factoryResetCount = 0;
			}
		}
	}
}

