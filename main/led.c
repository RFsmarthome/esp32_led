/*
 * led.c
 *
 *  Created on: 31.05.2017
 *      Author: schnake
 */

#include <math.h>
#include <stdint.h>

#include "driver/gpio.h"
#include "driver/rtc_io.h"
#include "driver/ledc.h"
#include "esp_attr.h"
#include "esp_err.h"
#include "esp_log.h"

#include "cJSON.h"
#include "coap.h"
#include "coap_helper.h"

#include "led.h"

float g_ledGamma = 2.8;
const int g_maxIn = 8191;
const int g_maxOut = 8191;
int g_maxBrightness = 8191;

#define MAX_LEDS 1

rgbw_t g_rgbw[MAX_LEDS];

void init_led()
{
	ESP_LOGD(__FUNCTION__, "init_led");

	for(int i=0; i<MAX_LEDS; ++i) {
		g_rgbw[i].r = g_rgbw[i].g = g_rgbw[i].b = g_rgbw[i].w = g_rgbw[i].brightness = 0;
	}

	ledc_timer_config_t ledc_timer = {
	        //set timer counter bit number
	        .bit_num = LEDC_TIMER_13_BIT,
	        //set frequency of pwm
	        .freq_hz = 1000,
	        //timer mode,
	        .speed_mode = LEDC_HIGH_SPEED_MODE,
	        //timer index
	        .timer_num = LEDC_TIMER_0
	};
	ledc_timer_config(&ledc_timer);

	ledc_channel_config_t ledc_channel_white = {
	        //set LEDC channel 0
	        .channel = CHANNEL_WHITE,
	        //set the duty for initialization.(duty range is 0 ~ ((2**bit_num)-1)
	        .duty = 0,
	        //GPIO number
	        .gpio_num = GPIO_WHITE,
	        //GPIO INTR TYPE, as an example, we enable fade_end interrupt here.
	        .intr_type = LEDC_INTR_DISABLE, //LEDC_INTR_FADE_END,
	        //set LEDC mode, from ledc_mode_t
	        .speed_mode = LEDC_HIGH_SPEED_MODE,
	        //set LEDC timer source, if different channel use one timer,
	        //the frequency and bit_num of these channels should be the same
	        .timer_sel = LEDC_TIMER_0
	};
	ledc_channel_config(&ledc_channel_white);

	ledc_channel_config_t ledc_channel_red = {
	        //set LEDC channel 0
	        .channel = CHANNEL_RED,
	        //set the duty for initialization.(duty range is 0 ~ ((2**bit_num)-1)
	        .duty = 0,
	        //GPIO number
	        .gpio_num = GPIO_RED,
	        //GPIO INTR TYPE, as an example, we enable fade_end interrupt here.
	        .intr_type = LEDC_INTR_DISABLE, //LEDC_INTR_FADE_END,
	        //set LEDC mode, from ledc_mode_t
	        .speed_mode = LEDC_HIGH_SPEED_MODE,
	        //set LEDC timer source, if different channel use one timer,
	        //the frequency and bit_num of these channels should be the same
	        .timer_sel = LEDC_TIMER_0
	};
	ledc_channel_config(&ledc_channel_red);

	ledc_channel_config_t ledc_channel_green = {
	        //set LEDC channel 0
	        .channel = CHANNEL_GREEN,
	        //set the duty for initialization.(duty range is 0 ~ ((2**bit_num)-1)
	        .duty = 0,
	        //GPIO number
	        .gpio_num = GPIO_GREEN,
	        //GPIO INTR TYPE, as an example, we enable fade_end interrupt here.
	        .intr_type = LEDC_INTR_DISABLE, //LEDC_INTR_FADE_END,
	        //set LEDC mode, from ledc_mode_t
	        .speed_mode = LEDC_HIGH_SPEED_MODE,
	        //set LEDC timer source, if different channel use one timer,
	        //the frequency and bit_num of these channels should be the same
	        .timer_sel = LEDC_TIMER_0
	};
	ledc_channel_config(&ledc_channel_green);

	ledc_channel_config_t ledc_channel_blue = {
	        //set LEDC channel 0
	        .channel = CHANNEL_BLUE,
	        //set the duty for initialization.(duty range is 0 ~ ((2**bit_num)-1)
	        .duty = 0,
	        //GPIO number
	        .gpio_num = GPIO_BLUE,
	        //GPIO INTR TYPE, as an example, we enable fade_end interrupt here.
	        .intr_type = LEDC_INTR_DISABLE, //LEDC_INTR_FADE_END,
	        //set LEDC mode, from ledc_mode_t
	        .speed_mode = LEDC_HIGH_SPEED_MODE,
	        //set LEDC timer source, if different channel use one timer,
	        //the frequency and bit_num of these channels should be the same
	        .timer_sel = LEDC_TIMER_0
	};
	ledc_channel_config(&ledc_channel_blue);

	ESP_LOGD(__FUNCTION__, "led channel configured");

	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_WHITE, 8191);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_RED, 8191);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_GREEN, 8191);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_BLUE, 8191);

	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_WHITE);
	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_RED);
	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_GREEN);
	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_BLUE);
}

void setLedGamma(float g)
{
	g_ledGamma = g;
}

float getLedGamma()
{
	return g_ledGamma;
}

void setLedRgbw(int index, const rgbw_t *rgbw) {
	g_rgbw[index].r = rgbw->r;
	g_rgbw[index].g = rgbw->g;
	g_rgbw[index].b = rgbw->b;
	g_rgbw[index].w = rgbw->w;
	g_rgbw[index].brightness = rgbw->brightness;
	ESP_LOGD(__FUNCTION__, "brightness: %d", g_rgbw[index].brightness);
}

const rgbw_t* getLedRgbw(const int index)
{
	return &g_rgbw[index];
}
void setLedRed(const int index, const int r)
{
	g_rgbw[index].r = r;
}
void setLedGreen(const int index, const int g) {
	g_rgbw[index].g = g;
}
void setLedBlue(const int index, const int b) {
	g_rgbw[index].b = b;
}
void setLedWhite(const int index, const int w) {
	g_rgbw[index].w = w;
}
void setLedBrightness(const int index, const int brightness) {
	g_rgbw[index].brightness = brightness;
}
void setLedOptions(const int index, const int options) {
	g_rgbw[index].options = options;
}

int getLedRed(const int index)
{
	return g_rgbw[index].r;
}
int getLedGreen(const int index)
{
	return g_rgbw[index].g;
}
int getLedBlue(const int index)
{
	return g_rgbw[index].b;
}
int getLedWhite(const int index)
{
	return g_rgbw[index].w;
}
int getLedBrightness(const int index)
{
	return g_rgbw[index].brightness;
}
int getLedOptions(const int index)
{
	return g_rgbw[index].options;
}

int getLedCount()
{
	return MAX_LEDS;
}

void setLedDuty(const int index)
{
	int duty;
	int maxOut = g_maxOut * g_rgbw[index].brightness / g_maxBrightness;

	duty = (pow((float)g_rgbw[index].w / (float)g_maxIn, g_ledGamma) * (float)maxOut + 0.5);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_WHITE, g_maxOut-duty);

	duty = (pow((float)g_rgbw[index].r / (float)g_maxIn, g_ledGamma) * (float)maxOut + 0.5);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_RED, g_maxOut-duty);

	duty = (pow((float)g_rgbw[index].g / (float)g_maxIn, g_ledGamma) * (float)maxOut + 0.5);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_GREEN, g_maxOut-duty);

	duty = (pow((float)g_rgbw[index].b / (float)g_maxIn, g_ledGamma) * (float)maxOut + 0.5);
	ledc_set_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_BLUE, g_maxOut-duty);

	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_WHITE);
	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_RED);
	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_GREEN);
	ledc_update_duty(LEDC_HIGH_SPEED_MODE, CHANNEL_BLUE);
}
