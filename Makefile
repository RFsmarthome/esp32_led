#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := esp32_led
GIT_VERSION := $(shell git describe --dirty --always --tags)

include $(IDF_PATH)/make/project.mk

CFLAGS := -DVERSION=\"$(GIT_VERSION)\" $(CFLAGS)
# CXXFLAGS := -DVERSION=\"$(GIT_VERSION)\" $(CXXFLAGS)
